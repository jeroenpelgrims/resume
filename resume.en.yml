contact:
    name: Jeroen Pelgrims
    address:
        street: Karel Govaertsstraat
        number: 16
        zip: 2100
        city: Deurne
        country: Belgium
    mobile: +32 4 9341 0991
    email: resume@jeroenpelgrims.com
    website: https://jeroenpelgrims.com
    linkedin: jeroenpelgrims
    gitlab: jeroenpelgrims
    github: jeroenpelgrims
    tagline: ""
about: |
    I'm a software developer from Antwerp, Belgium who's lived in Stavanger, Norway for 4.5 wonderful years.
    As a developer, I'm a generalist and have experience with both frontend and backend development.
    I love trying out any new technology that arises and am particularly fond of anything to do with webdevelopment.<br>
    I have contributed to and am interested in FOSS.
experience:
    -
        title: Software development consultant
        company: Self-employed
        site: https://digicraft.eu
        from: 2022-10-01
        to: 
        description: |
            Full stack software developer
        location: Worldwide
    -
        title: Senior Developer (freelance)
        company: SEAM
        site: https://www.seam.no/
        from: 2023-10-01
        to: 2024-01-12
        location: Haugesund, Norway (Remote)
        description: |
            Plotting historical data from equipment on a large vessel to graphs using Vue & Chart.js
            Improving the performance of the application by optimizing the InfluxDB queries
        skills:
            - Typescript
            - Vue
            - Electron
            - Node.js
            - InfluxDB
            - Chart.js
    -
        title: Senior Frontend Developer (freelance)
        company: DMaze (Proactima)
        site: https://www.dmaze.com/
        from: 2023-06-01
        to: 2023-09-30
        location: Stavanger, Norway (Remote)
        description: |
            I assisted the internal development team in implementing a solution to manage taxonomies that have a dynamic data format.
            This involved using React Query and Tanstack Table to implement a dynamic, editable table.
            The UI was implemented using a customized Chakra UI component library.
        skills:
            - Typescript
            - React
            - Jest
    -
        title: Senior Developer & Architect (freelance)
        company: Campall AS
        site: https://www.linkedin.com/company/campall-as/
        from: 2022-10-01
        to: 2023-05-31
        location: Stavanger, Norway (Remote)
        description: |
            Liaison between client and the rest of the development team, prioritizing based on time & budget constraints.  
            Guide junior backend developer. Chose technologies based on the scalability requirements of the client.
            Developed the frontend part of the application and integration with payment solutions.
        skills:
            - Next.js
            - React
            - Responsive design
            - REST
    -
        title: Secondary school teacher (IT) & ICT coordinator
        company: Scheppersinstituut Deurne & Antwerpen
        site: https://scheppers.be
        from: 2019-09-01
        to: 2022-08-31
        location: Antwerp, Belgium
        description: |
            Teaching & evaluating basic IT skills to students aged 13-16. 130 students yearly.
            Part of a small team of 3-4 people managing the IT infrastructure across 3 campuses.
    -
        title: Software developer & Mentor
        company: Take the Lead
        site: https://takethelead.be
        from: 2019-01-14
        to: 2019-05-31
        location: Antwerp, Belgium
        description: |
            Software developer on an SPA using React + TS, mentoring junior developers & interns.
        skills:
            - Typescript
            - React
            - Redux
            - Git
    -
        title: Lead frontend developer (1.5y), Frontend Developer (3y)
        company: Altibox AS
        site: https://www.altibox.no
        from: 2014-09-01
        to: 2019-03-31
        location: Stavanger, Norway
        description: |
            Frontend developer on the Altibox website (http://altibox.no).
            - Created new and updated existing selfservice applications to either order new products or manage settings for owned products.
            - Streamlining/modernizing of development process bi introducing buid tools & templating, and later by introducing React.
            - Migrating the Altibox website CMS from Liferay to Wordpress.
            Became Lead Frontend Developer:
            - Implemented the new Web-TV for Altibox (<a href="https://tv.altibox.no">https://tv.altibox.no</a>) using Typescript & React
            - Introduced pull request workflow, main reviewer of frontend code
            - Mentoring co-workers in learning React & Typescript
        skills:
            - Typescript
            - React
            - Redux
            - Docker
            - Git
            - node.js
            - Javascript ES6
            - Twitter bootstrap
            - Wordpress
            - Handlebars.js
            - Gulp
    -
        title: Software developer
        company: Unbrace
        site: https://unbrace.be
        from: 2013-07-01
        to: 2014-08-31
        location: Antwerp, Belgium
        description: |
            All-round developer on BVC Spot Buy Center, implemented large parts of the Javascript frontend application and RESTful webservices backend.
        skills:
            - ServiceStack (.NET)
            - Backbone
            - Backbone.Marionette
            - RavenDB
            - RequireJS
            - HTML5
            - CSS3
            - Twitter bootstrap
            - responsive design
            - Git
    -
        title: Software developer
        company: Item Solutions
        site: http://itemsolutions.com
        from: 2011-09-01
        to: 2013-06-30
        location: Antwerp, Belgium
        description: |
            Main developer on Aertssen WOP Drag & Drop, a time schedule tool for a contractor who is active worldwide.
            Team member on various other projects.
        skills:
            - .NET (3.5, 4, 4.5)
            - Silverlight (3-5)
            - T-SQL
            - Oracle
            - (N)Hibernate
            - Dapper
            - RhinoMocks
            - Moq
            - KnockoutJS
            - RequireJS
            - jQuery
            - Spring
            - Spring MVC
            - SVN
education:
    -
        title: Bachelor in Secondary Education (Reduced duration)
        company: Karel de Grote University College
        site: https://www.kdg.be
        from: 2019-09-01
        to: 2022-06-30
        location: Antwerp, Belgium
        skills:
            - Didactics
            - Creating lesson plans
    -
        title: Bridge program for Master in applied computer science (Unfinished)
        company: Vrije Universiteit Brussel
        site: https://www.vub.ac.be
        from: 2010-09-01
        to: 2011-06-31
        location: Brussels, Belgium
        skills:
            - Structure and Interpretation of Computer Programs (Scheme)
            - Algorithms and Data Structures
            - Artificial Intelligence
    -
        title: Bachelor in applied computer science
        company: Karel de Grote University College
        site: https://www.kdg.be
        from: 2007-09-01
        to: 2010-06-30
        location: Antwerp, Belgium
        skills:
            - Structured programming (C)
            - Object oriented programming (Java)
            - Networks (CISCO)
            - SQL & Database design
            - Hibernate
            - Hypermedia (html & javascript)
            - ASP.net (C#)
languages:
    Dutch: C2
    English: C2
    Norwegian: C1
    French: B1
    Swedish: Can understand
    Danish: Can read
courses:
    - 
        title: Functional Programming Principles in Scala
        school: Ecole Polytechnique Fédérale de Lausanne
        site: https://www.coursera.org/learn/progfun1
        from: 2012-09-01
        to: 2012-12-01
        score: 100%
    - 
        title: Introduction to Functional Programming in OCaml
        school: Université Paris Diderot
        site: https://www.fun-mooc.fr/courses/course-v1:parisdiderot+56002+session03/about
        from: 2018-11-30
        to: 2018-09-01
        score: 99%
    - 
        title: Basic Life Support Adults + AED usage
        site: https://www.lifeguard.be/opleidingen/basic-life-support-volwassenen-plus-aed/
        school: Lifeguard
        from: 2019-03-01
        to: 2019-03-01
        score: 4 hours course
    
hobbies: |
    Besides at my work I also love programming in my spare time.
    For some examples of code I've written you can take a look at my <a href="https://gitlab.com/jeroenpelgrims">Gitlab</a> or <a href="https://github.com/resurge">Github</a> accounts.
    Links to these can be found on my personal website if you're holding the paper version of my resume
    (the pdf of which is automatically generated from a yaml file after pushing to my resume repository :)).<br>
    <br>
    Besides sitting behind the computer I've also hitchhiked about 5000 km accross Europe and am an amateur gardener.<br>
    I love hiking and have hiked some of Norway's best known landmarks like Preikestolen, Kjeragbolten and Trolltunga.<br>
    Sometimes I try out new things just because they sound interesting like pinhole photography, salsa dancing or brewing my own beer.