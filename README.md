# Resume repository of [Jeroen Pelgrims](http://jeroenpelgrims.com)

This repository contains my resume in YAML format.  
It will automatically generate a pdf file after being pushed to.

## Generating the pdf locally:
- `npm i`
- `npm run install:fonts` (Run the contents of the command with sudo before the cp command)
- `npm run build`