import * as yargs from 'yargs';
import * as ejs from 'ejs';
import * as fs from 'fs';
import * as htmlPdf from 'html-pdf';
import * as moment from 'moment';
import mergeConfigs from './mergeConfigs';
import { readYamlFile } from './yaml-util';
import i18n from './i18n';

async function generateResume(language: string) {
	const base = readYamlFile('resume.en.yml');
	const langSpecific = language === 'en'
		? {}
		: readYamlFile(`resume.${language}.yml`);
	const config = mergeConfigs(base, langSpecific);

	if (langSpecific.languages) {
		(config as any).languages = langSpecific.languages;
	}
	
	const css = fs.readFileSync(__dirname + '/template/style.css').toString();
	const t = await i18n(language);

	ejs.renderFile(__dirname + '/template/index.ejs', { ...config, css, moment, t }, {}, (err, html) => {
		if (err) {
			console.log('Error occurred while rendering template:', err);
			process.exit(1);
		}

		const pdf = htmlPdf.create(html, { format: 'A4' });
		pdf.toFile(`resume.${language}.pdf`, (err) => {
			if (err) {
				console.log('Error while converting html to pdf:', err);
				process.exit(1);
			}
		});
	});
}

const language = yargs.argv.lang || 'en';
if (!language) {
	console.error('The "lang" parameter must be set');
	process.exit(1);
}

(async () => {
	await generateResume(language);
})();