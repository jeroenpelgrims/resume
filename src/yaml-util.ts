import * as yaml from 'js-yaml';
import * as fs from 'fs';

export function readYamlFile(file: string) {
	return yaml.safeLoad(fs.readFileSync(file).toString());
}