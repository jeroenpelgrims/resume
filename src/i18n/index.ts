import { readYamlFile } from '../yaml-util';

export default async (lng: string) => require('i18next').init({
	lng,
	debug: false,
	resources: {
		[lng]: {
			translation: readYamlFile(__dirname + `/${lng}.yml`)
		}
	}
});